angular.module('msl-search-plugin', [])
.directive('mslSearchField', ['$rootScope', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, iAttrs) {
            element.bind('keydown', function(e) {
                if(e.keyCode == 38) { 
                    e.preventDefault();
                    $rootScope.$emit('msl-search:select-item', 'up');
                }
                else if(e.keyCode == 40){
                    e.preventDefault();
                    $rootScope.$emit('msl-search:select-item', 'down');
                }
                else if(e.keyCode == 13){
                    e.preventDefault();
                    $rootScope.$emit('msl-search:enter-item');
                }
            });
            scope.onSearchFieldFocus = function(){
                $rootScope.$emit('msl-search:show-items');
            };
        }
    };
}])
.directive('mslSearchPlugin', ['$rootScope', '$http', '$document', '$timeout', function ($rootScope, $http, $document, $timeout) {
    return {
        restrict: 'A',
        scope: {
            value: '=mslSearchPlugin'
        },
        template: ["<div ng-show='data.isPluginVisible'>",
            "<div class='msl-search-item' ng-repeat='user in data.foundUsers' ng-class='{selected: $index == data.selectedIndex}' ng-click='goToUser($index)'>",
                "<div class='user-image' ng-style='user.imageStyle'></div>",
                "<div class='user-info'>",
                    "<div class='user-names' ng-bind='user.names'></div>",
                    "<div class='user-job' ng-bind='user.job'></div>",
                "</div>",
            "</div>",
        "</div>"].join(''),
        link: function (scope, element, iAttrs) {
            scope.data = {
                foundUsers: [],
                selectedIndex: 0,
                isPluginVisible: false
            };
            $rootScope.$on('msl-search:select-item', function(event, direction){
                var allUser = scope.data.foundUsers;
                scope.$apply(function(){
                    if(direction === 'down'){
                        if (scope.data.selectedIndex < allUser.length - 1) {
                            ++scope.data.selectedIndex;
                        }
                    }
                    else if(direction === 'up'){
                        if (scope.data.selectedIndex > 0) {
                            --scope.data.selectedIndex;
                        }
                    }
                });
            });
            $rootScope.$on('msl-search:show-items', function(event){
                $timeout(function(){
                    scope.data.isPluginVisible = true;
                }, 500, true);
            });
            $rootScope.$on('msl-search:enter-item', function(event){
                if (scope.data.isPluginVisible === true) {
                    scope.goToUser();
                }
            });
            $document.on('click', function(event){
                scope.$apply(function(){
                    scope.data.isPluginVisible = false;
                });
                console.log('$document click event', event, scope.data.foundUsers);
            });
            scope.goToUser = function(idx){
                var allUser = scope.data.foundUsers,
                    index = idx ? idx : scope.data.selectedIndex;
                alert('#' + index + ': ' + allUser[index].url);
            };
            function doSearch(value){
                console.log('value', value, value.length);
                if (value.length > 2) {
                    $http.get('/site/search/search-user', {
                        params: {
                            value: value
                        }
                    }).then(function(response){
                        console.log('response', response, arguments);
                        var output = [];
                        angular.forEach(response.data.users, function(user){
                            if (user.image !== null) {
                                user.imageStyle = {
                                    'background-image': 'url(' + user.image + ')'
                                };
                            }
                            output.push(user);
                        });
                        scope.data.foundUsers = output;
                        scope.data.isPluginVisible = true;
                        console.log('scope.foundUsers', scope.data, output);

                    });
                }
                else if(scope.data.foundUsers.length > 0){
                    scope.data.foundUsers.length = [];
                }
            }
            scope.$watch('value', function(newSearchValue){
                doSearch(newSearchValue);
            });
        }
    };
}])
.controller('SearchCtrl', ['$scope', function($scope){
    $scope.data = {
        search: '',
    };
}]);