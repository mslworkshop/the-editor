angular.module('MslEditor', [
    'angularFileUpload',
    'ui.bootstrap.tpls',
    'ui.bootstrap',
    'textAngular',
    'ngSanitize',
    'colorpicker.module',
    'ngTagsInput',
    'msl-search-plugin',
    'isteven-multi-select'
])
.config(['$sceDelegateProvider', '$locationProvider', function($sceDelegateProvider, $locationProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://www.youtube.com/**',
        '//www.youtube.com/**'
    ]);
    // $locationProvider.html5Mode(true);
    //www.youtube.com/embed/arKuicYU3FU
}])
/*.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}])*/
.directive('onHover', [function () {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            scope.projectItemHover = function(eventType){
                scope.showHoverBox = false;
                if (eventType === 'enter') {
                    scope.showHoverBox = true;
                }
                else{
                    scope.showHoverBox = false;
                }
            };
        }
    };
}])
.directive('follow', ['$window', '$timeout', function ($window, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, iAttrs) {

            angular.element($window).bind("scroll", function() {
                var el = element[0],
                    topOffset = $window.scrollY;

                if ($window.scrollY > 95) {
                    element.css({
                        left: 0,
                        top: ($window.scrollY - 95)+'px',
                        position: 'absolute',
                        transition: 'top .5s',
                        'padding-right': 'inherit'
                    });
                }
                $timeout(function(){
                    if ($window.scrollY <= 95) {
                        element.css('top', 0);
                    }
                }, 100);

            });
        }
    };
}])
.service('DataManager', ['$http', function ($http) {
    return {
        post: function(url, data){
            if (angular.isUndefined(data)) {
                data = {};
            }
            return $http.post(url, data).then(function(response) {
                return response.data;
            });
        },
        getProjectData: function(userId){
            return $http.post('/project/get/' + userId).then(function(response) {
                return response.data;
            });
        },
        getCategories: function(){
            return $http.post('/admin/categories/get').then(function(response) {
                return response.data;
            });
        },
        getPublicCategories: function(){
            return $http.post('/admin/categories/get').then(function(response) {
                return response.data;
            });
        },
        saveCategories: function(saveData){
            return $http.post('/admin/categories/save', saveData).then(function(response) {
                return response.data;
            });
        }

    };
}])
.controller('MainCtrl', ['$scope', '$rootScope', '$window', 
                        '$sce', '$modal', '$upload', '$timeout', '$http', '$location', 'DataManager',
function($scope, $rootScope, $window, $sce, $modal, $upload, $timeout, $http, $location, DataManager){
    if ($(window).width() <= 768) {
        $scope.isMobile = true;
    }
    else{
        $scope.isMobile = false;
    }
    if ($scope.isMobile === true) {
        var modalInstance = $modal.open({
            templateUrl: '/public/tpls/modal.tpl.html',
            controller: ['$scope', '$modalInstance', 'config', function($scope, $modalInstance, config){
                $scope.config = config;
                $scope.goBack = function(){
                    $modalInstance.dismiss('cancel');
                };
            }],
            size: 'sm',
            resolve: {
                config: function(){
                    return {
                        type: 'message',
                        modalTitle: 'Not available',
                        message: 'This feature is not available under mobile yet.'
                    };
                }
            }
        });
        modalInstance.result.then(function () {

        }, function () {
            window.history.back();
        });
    }
    $scope.cfg = {
        /*inputList: [
            { firstName: "Peter",    lastName: "Parker",    selected: false },
            { firstName: "Mary",     lastName: "Jane",   selected: false },
            { firstName: "David",    lastName: "Banner",    selected: false },
            { firstName: "Bruce",    lastName: "Wayne",     selected: true  },
            { firstName: "Natalia",  lastName: "Romanova",  selected: false },
            { firstName: "Clark",    lastName: "Kent",  selected: true  }
        ],*/
        output: []
    };
    var cfg = {
        isProjectEdit: false,
        projectEditData: {}
    };
    $scope.input = {
        currentStep: 1,
        defaultTitle: 'Untitled project',
        userNames: '',
        isProjectEdit: true,
        categoriesList: [],
        errorFields: {}
    };
    $scope.data = {
        formId: new Date().getTime(),
        projectTitle: null,
        cover: '',
        background: '#fff',
        backgroundType: 'color',
        category: [],
        tags: [],
        description: '',
        teams: [],
        extraInfo: {
            brand: [],
            agency: [],
            school: []
        },
        credits: [],
        copyright: '',
        tools: [],
        items: [],
        isDraft: false,
        adultContent: false
    };
    function getIsSelectedCategory(ctg){
        var match = false;
        angular.forEach($scope.data.category, function(category){
            if (category.categoryName === ctg) {
                match = true;
                return true;
            }
        });
        return match;
    }
    $rootScope.$on('msl-load-categories', function(){
        DataManager.getCategories().then(function(categoriesList){
            var output = [];
            angular.forEach(categoriesList, function(category){
                output.push({
                    categoryId: category.id,
                    categoryName: category.name,
                    isSelected: getIsSelectedCategory(category.name)
                });
            });
            $scope.input.categoriesList = output;
        });
    });
    $http.post('/xhr/get_logged_user', {
            formId: $scope.data.formId,
            acess: '8cf205e11d5eb3c998ac34958304c608'
        }, {
            headers: {
                'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
            },
            params: {

                token: '8cf205e11d5eb3c998ac34958304c608',
            }
    }).then(function(response) {
        $scope.input.userNames = response.data.user.Name + ' ' +response.data.user.Lastname;
        $scope.data.credits.push({
            user: $scope.input.userNames
        });
        console.log('get_logged_user', response, response.data.user.Name, response.data.user.Lastname);
    });
    var segments = $location.absUrl().split("/"),
        isEdit = segments[segments.length - 1] || false;

    if (parseInt(isEdit, 10)) {
        var id = parseInt(isEdit, 10);
        DataManager.getProjectData(id).then(function(response){
            if (response.error) {
                alert(response.error);
                window.location = "/site";
            }
            else{
                $scope.data = response.post.data;
                $scope.data.formId = new Date().getTime();
                cfg.isProjectEdit = true;
                cfg.projectEditData.projectId = id;

                $rootScope.$emit('msl-load-categories');
            }
            console.log('get data', $scope.data, isEdit);
        });
    }
    else{
        $rootScope.$emit('msl-load-categories');
    }
    /*$scope.$watchCollection('data', function(data){
        console.log('watch', data)
    });*/
    // window.d = $scope.data;
    $scope.temp = {
        items: []
    };
    $scope.config = {
        textAngularToolbar: [
            ['h1','h2','h3', 'h4', 'h5', 'h6', 'p'],
            ['bold', 'italics', 'underline'],
            ['ul', 'ol'],
            ['justifyCenter', 'justifyLeft', 'justifyRight'],
            ['insertLink','fontColor']
        ],

    };
    $rootScope.closeModal = function(modalInstance){
        modalInstance.dismiss('cancel');
        $scope.input.currentStep = 1;
    };
    function addItem(itemData){
        $scope.data.items.push(itemData);
    }
    $scope.setCover = function(){
        $scope.input.currentStep = 2;
        var modalInstance = $modal.open({
            templateUrl: '/public/tpls/modal.tpl.html',
            controller: ['$scope', '$modalInstance', 'data', 'input', 'config', function($scope, $modalInstance, data, input, config){
                console.log('data', data);
                $scope.data = data;
                $scope.input = input;
                $scope.config = config;
                $scope.edit = {
                    projectTitle: data.projectTitle
                };
                $scope.setCoverImage = function($files){
                    if ($files && $files.length) {
                        $upload.upload({
                            url: '/xhr/posts/attach_files',
                            headers: {
                                'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
                            },
                            data: {
                                formId: $scope.data.formId,
                                token: '8cf205e11d5eb3c998ac34958304c608',
                                acess: '8cf205e11d5eb3c998ac34958304c608'
                            },
                            file: $files[0]
                        }).success(function (data, status, headers, config) {
                            $scope.data.cover = data.image.large_url;
                        });
                        /*//Temp
                        $scope.data.cover = 'http://upload.wikimedia.org/wikipedia/commons/c/c1/Bare_mountain,_nevada.jpg';
                        $scope.data.backgroundType = 'image';*/
                    }
                };
                $scope.save = function(){
                    data.projectTitle = $scope.edit.projectTitle;
                    $rootScope.closeModal($modalInstance);
                };
                $scope.cancel = function(){
                    $rootScope.closeModal($modalInstance);
                };
            }],
            size: 'sm',
            resolve: {
                data: function () {
                    return $scope.data;
                },
                input: function () {
                    return $scope.input;
                },
                config: function(){
                    return {
                        type: 'set-cover',
                        modalTitle: 'Choose cover',
                        defaultTitle: $scope.input.defaultTitle
                    };
                }
            }
        });
        modalInstance.result.then(function () {

        }, function () {
            $scope.input.currentStep = 1;
            $timeout(function(){
                $scope.setSettings();
            }, 500, true);
        });
    };
    $scope.setSettings = function(){
        $scope.input.currentStep = 3;
        var modalInstance = $modal.open({
            templateUrl: '/public/tpls/modal.tpl.html',
            controller: ['$scope', '$modalInstance', 'data', 'input', 'config', function($scope, $modalInstance, data, input, config){
                console.log('data-set', data);
                $scope.data = data;
                $scope.input = input;
                $scope.config = config;
                $scope.addCredits = function(){
                    $scope.data.credits.push({
                        label: 'Owner',
                        user: 'Marin Lazarov'
                    });
                };
                angular.forEach($scope.input.categoriesList, function(category){
                    category.isSelected = getIsSelectedCategory(category.categoryName);
                });
                $scope.beforeTagAdd = function(tag){
                    var idx = $scope.data.tags.indexOf(tag);
                    if (angular.isDefined($scope.data.tags[idx])) {
                        var letters = $scope.data.tags[idx].text.split('');
                        angular.forEach(letters, function(letter, index){
                            if (letter === '#') {
                                letters.splice(index, 1);
                            }
                        });
                        $scope.data.tags[idx].text = letters.join('');
                    }
                };
                $scope.save = function(){
                    // data.projectTitle = $scope.edit.projectTitle;
                    $rootScope.closeModal($modalInstance);
                };
                $scope.cancel = function(){
                    $rootScope.closeModal($modalInstance);
                };
            }],
            size: 'lg',
            resolve: {
                data: function () {
                    console.warn('$scope.data-data', $scope.data);
                    return $scope.data;
                },
                input: function () {
                    return $scope.input;
                },
                config: function(){
                    return {
                        type: 'set-settings',
                        modalTitle: 'Settings'
                    };
                }
            }
        });
        modalInstance.result.then(function () {
        }, function () {
            console.log('$scope.data', arguments);
            $scope.input.currentStep = 1;
        });
    };
    $scope.editProjectTitle = function(){
        var modalInstance = $modal.open({
            templateUrl: '/public/tpls/modal.tpl.html',
            controller: ['$scope', '$modalInstance', 'data', 'config', function($scope, $modalInstance, data, config){
                console.log('data', data);
                // $scope.data = data;
                $scope.edit = {
                    projectTitle: data.projectTitle
                };
                $scope.config = config;
                $scope.save = function(){
                    data.projectTitle = $scope.edit.projectTitle;
                    $rootScope.closeModal($modalInstance);
                };
                $scope.cancel = function(){
                    $rootScope.closeModal($modalInstance);
                };
            }],
            size: 'sm',
            resolve: {
                data: function () {
                    return $scope.data;
                },
                config: function(){
                    return {
                        type: 'edit-title',
                        modalTitle: 'Edit project title',
                        defaultTitle: $scope.input.defaultTitle
                    };
                }
            }
        });
    };
    /*window.addEventListener("beforeunload", function (e) {
      var confirmationMessage = "\o/";

      (e || window.event).returnValue = confirmationMessage;     //Gecko + IE
      return true;                                //Gecko + Webkit, Safari, Chrome etc.
    });*/
    function clearTemp(){
        // $scope.temp.items.length = 0;
        angular.forEach($scope.data.items, function(item, index){
            if (item.isActiveEdit === true) {
                $scope.data.items.splice(index, 1);
                return false;
            }
        });
        $scope.addItemError = null;
    }
    function getElementTop ( Elem ) {
        var elem = Elem;

        yPos = elem.offsetTop;
        tempEl = elem.offsetParent;

        while ( tempEl != null ) 
        {
            yPos += tempEl.offsetTop;
            tempEl = tempEl.offsetParent;
        }  

        return yPos;
    }
    function goToBottom(){
        $timeout(function(){
            var el = document.getElementsByClassName('last-item');
            if (el.length === 1) {
                var top = getElementTop(el[0]);
                window.scrollTo(0, top);
            }
        });
    }
    $scope.onFileSelect = function($files, $event){
        if ($files && $files.length) {
            $upload.upload({
                url: '/xhr/posts/attach_files',
                headers: {
                    'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
                },
                data: {
                    formId: $scope.data.formId,
                    token: '8cf205e11d5eb3c998ac34958304c608',
                    acess: '8cf205e11d5eb3c998ac34958304c608'
                },
                file: $files[0]
            }).success(function (data, status, headers, config) {
                console.log('data', data);
                $scope.data.items.push({
                    type: 'file',
                    content: "<img class='project-item-image' src='" + data.image.large_url + "'/>",
                    isActiveEdit: false
                });
            });
        }
    };
    $scope.setBackgroundImage = function($files, $event){
        if ($files && $files.length) {
            $upload.upload({
                url: '/xhr/posts/attach_files',
                headers: {
                    'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
                },
                data: {
                    formId: $scope.data.formId,
                    token: '8cf205e11d5eb3c998ac34958304c608',
                    acess: '8cf205e11d5eb3c998ac34958304c608'
                },
                file: $files[0]
            }).success(function (data, status, headers, config) {
                $scope.data.background = data.image.large_url;
                $scope.data.backgroundType = 'image';
            }); 
        }
    };
    $scope.setBackgroundColor = function(){
        console.log('setBackgroundColor');
        $scope.data.backgroundType = 'color';
    };
    $rootScope.getBackground = function(){
        if ($scope.data.backgroundType === 'image') {
            return "url('"+$scope.data.background+"')";
        }
        else{
            return $scope.data.background;
        }
    };
    $rootScope.parseHtml = function(index){
        return $sce.trustAsHtml($scope.data.items[index].content);
    };
    $scope.addEmbedMedia = function(){
        clearTemp();
        $scope.data.items.push({
            type: 'embed',
            content: '',
            isActiveEdit: true
        });
        goToBottom();
    };
    $scope.addText = function(){
        // alert('Add text');
        clearTemp();
        $scope.data.items.push({
            type: 'text',
            content: '',
            isActiveEdit: true
        });
        goToBottom();
    };
    function isValidUrl(){ 
        return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(arguments[0]); 
    }
    function getDomain(url){
        var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
        return matches[1];
    }
    function getVimeoId(url) {
        vimeoRegExp = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
        var match = url.match(vimeoRegExp);
      
        if (match){
            return match[3];
        }
        else{
            return false;
        }
    }
    $scope.saveItem = function(index){

        // var tempItem = angular.copy($scope.temp.items[index]);
        // $scope.temp.items.splice(index, 1);
        // $scope.data.items.push(tempItem);
        $scope.addItemError = null;
        if ($scope.data.items[index].type === 'embed') {
            var content = $scope.data.items[index].content;

            if(content.length > 3 && isValidUrl(content)){
                var domain = getDomain(content),
                    embedUrl = false;
                if (domain.indexOf('youtube.com') !== -1) {
                    var expr = /(?:v=)([^&]+)/,
                        matches = content.match(expr),
                        videoId = false;

                    if (angular.isDefined(matches[1])) {
                        videoId = matches[1];
                        embedUrl = 'https://www.youtube.com/embed/' + videoId;
                    }
                }
                else if(getVimeoId(content)){
                    embedUrl = 'https://player.vimeo.com/video/' + getVimeoId(content);
                }
                if (embedUrl !== false) {
                    iFrameWidth = 523;
                    iFrameHeight = iFrameWidth / 4 * 3;
                    $scope.data.items[index].content = "<iframe width='"+iFrameWidth+"' height='"+iFrameHeight+"' src='"+embedUrl+"' frameborder='0' allowfullscreen></iframe>";
                    $scope.data.items[index].isActiveEdit = false;

                    return;
                }
            }
            var element = angular.element($scope.data.items[index].content);
            if ((element.length === 1) && (element[0].tagName.toLowerCase() === 'iframe')) {
                iFrameWidth = 523;
                $scope.data.items[index].content = "<iframe width='"+iFrameWidth+"' height='"+element.attr('height')+"' src='"+element.attr('src')+"' frameborder='0' allowfullscreen></iframe>";
                $scope.data.items[index].isActiveEdit = false;
            }
            else{

                $scope.addItemError = 'Add valid embed code, please.';
            }
        }
        else{
            $scope.data.items[index].isActiveEdit = false;
        }
    };
    $scope.cancelItem = function($event){
        $event.preventDefault();
        clearTemp();
    };
    /*$scope.containerClick = function($event){
        console.log('containerClick', $event);
        if ($event.offsetX  <= 84 || $event.offsetY <= 25) {
            console.log('Src');
        }
    };*/
    Array.prototype.moveUp = function(value, by) {
        var index = this.indexOf(value),     
            newPos = index - (by || 1);
         
        if(index === -1) 
            throw new Error('Element not found in array');
         
        if(newPos < 0) 
            newPos = 0;
             
        this.splice(index,1);
        this.splice(newPos,0,value);
    };
    Array.prototype.moveDown = function(value, by) {
        var index = this.indexOf(value),     
            newPos = index + (by || 1);
        
        if(index === -1) 
            throw new Error("Element not found in array");
        
        if(newPos >= this.length) 
            newPos = this.length;
        
        this.splice(index, 1);
        this.splice(newPos,0,value);
    };
    $scope.moveItem = function(direction, index){
        if (direction === 'up') {
            $scope.data.items.moveUp($scope.data.items[index]);
        }
        else if (direction === 'down') {
            $scope.data.items.moveDown($scope.data.items[index]);
        }
    };
    $scope.editItem = function(index){
        $scope.data.items[index].isActiveEdit = true;
    };
    $scope.removeItem = function(index){
        $scope.data.items.splice(index, 1);
    };
    $scope.showPreview = function(){
        // $scope.input.currentStep = 3;
        var modalInstance = $modal.open({
            templateUrl: '/public/tpls/preview.tpl.html',
            controller: ['$scope', '$modalInstance', 'data', 'input', 'config', function($scope, $modalInstance, data, input, config){
                console.log('data-set', data);
                $scope.data = data;
                $scope.input = input;
                $scope.config = config;
                /*$scope.addCredits = function(){
                    $scope.data.credits.push({
                        label: 'Owner',
                        user: 'Marin Lazarov'
                    });
                };*/
                $scope.cancel = function(){
                    $rootScope.closeModal($modalInstance);
                };
            }],
            size: 'lg',
            resolve: {
                data: function () {
                    return $scope.data;
                },
                input: function(){
                    return $scope.input;
                },
                config: function(){
                    return {
                        type: 'set-settings',
                        modalTitle: 'Settings'
                    };
                }
            }
        });
        modalInstance.result.then(function () {
        }, function () {
            $scope.input.currentStep = 1;
        });
    };
    $rootScope.saveProject = function(isPublish){
        console.log('saveProject', $scope.data, isPublish);
        var saveData = {
                data: $scope.data
            },
            tempCategories = [],
            validation = {
                projectTitle: 'string',
                cover: 'string',
                category: 'array'/*,
                tools: 'array',
                description: 'string'*/
            },
            isValid = true;
        $scope.input.errorFields = {};
        saveData.data.isPublish = isPublish;
        angular.forEach(validation, function(type, item){
            if (angular.isDefined(saveData.data[item])) {
                var check = saveData.data[item];
                if (type === 'string' && (check === null || check === '')) {
                    isValid = false;
                    $scope.input.errorFields[item] = true;
                    console.log('item', item);
                }
                else if(type === 'array' && check.length === 0){
                    isValid = false;
                    $scope.input.errorFields[item] = true;
                    console.log('item', item);
                }
            }
        });
        angular.forEach($scope.data.category, function(ctg){
            tempCategories.push({
                categoryName: ctg.categoryName
            });
        });
        saveData.data.category = tempCategories;
        if (isValid === true) {
            var postProjectUrl = '/project/save/';
            if (cfg.isProjectEdit === true && angular.isDefined(cfg.projectEditData) && angular.isDefined(cfg.projectEditData.projectId)) {
                postProjectUrl += cfg.projectEditData.projectId;
            }
            DataManager.post(postProjectUrl, saveData).then(function(response){
                // alert('The project has been added');
                // console.log('Saved', response.post);
                window.location = '/#project-' + response.post.id;
            });
        }
        else{
            if (saveData.data.cover === '' || saveData.data.projectTitle === '') {
                $scope.setCover();
            }
            else if(saveData.data.category.length === 0){
                $scope.setSettings();
            }
        }
    };
    // $scope.remove
}])
.controller('CategoriesCtrl', ['$scope', 'DataManager', function($scope, DataManager){

    $scope.cfg = {
        isSaving: false
    };
    $scope.data = {
        categories: [],
        deletedIds: []
    };
    DataManager.getCategories().then(function(categoriesList){
        $scope.data.categories = categoriesList;
    });
    $scope.removeCategory = function(index){
        var category = $scope.data.categories[index];
        if (angular.isDefined(category)) {
            $scope.data.deletedIds.push(category.id);
            $scope.data.categories.splice(index, 1);
        }
    };
    $scope.addCategory = function(){
        $scope.data.categories.push({
            id: null,
            name: ''
        });
    };
    $scope.saveCategories = function(){
        $scope.cfg.isSaving = true;
        DataManager.saveCategories($scope.data).then(function(response){
            // alert(123);
            $scope.cfg.isSaving = false;
        });
    };
}]);
